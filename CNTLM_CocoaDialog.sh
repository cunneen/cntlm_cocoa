#!/bin/bash

CD_APP="CocoaDialog.app"
CD="$CD_APP/Contents/MacOS/CocoaDialog"

### run CNTLM
rv=(`$CD secure-standard-inputbox --title "Type your proxy password for ${LOGNAME}" \
                             --text ""  \
                             --informative-text "Make sure you get this right or you'll be logged out!" \
                             --float`)

./cntlm -f -c /dev/null -l 3128 -u $LOGNAME -d SEMC_CLIENT -p ${rv[1]} proxy:8080

# $CD bubble --debug --title "CNTLM is running"        \
#	--text "Attempted to open CNTLM proxy on localhost port 3128"  \
#	--border-color  "a25f0a"                 \
#	--text-colors  "000000"                   \
#	--background-top  "dfa723"               \
#	--background-bottom  "fdde88"            \
#	--icon-file "$CD_APP/Contents/Resources/globe.icns"